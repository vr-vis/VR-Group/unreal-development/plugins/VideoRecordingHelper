// Fill out your copyright notice in the Description page of Project Settings.


#include "Camera.h"

#include "SlateMaterialBrush.h"
#include "Components/Image.h"
#include "Components/SceneCaptureComponent2D.h"
#include "Engine/TextureRenderTarget2D.h"
#include "Engine/UserInterfaceSettings.h"
#include "Widgets/Images/SImage.h"

// Sets default values
ACamera::ACamera()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>("Root"));

	CameraMesh = CreateDefaultSubobject<UStaticMeshComponent>("CameraMesh");
	CameraMesh->SetupAttachment(GetRootComponent());

	SceneCapture = CreateDefaultSubobject<USceneCaptureComponent2D>("SceneCapture");
	SceneCapture->SetupAttachment(GetRootComponent());
	SceneCapture->bCaptureOnMovement = false;
	SceneCapture->bCaptureEveryFrame = true;
	SceneCapture->HiddenActors.Add(this);

	static ConstructorHelpers::FObjectFinder<UMaterial> CameraDisplayMaterialFinder(TEXT("Material'/VideoRecordingHelper/CameraDisplayMaterial.CameraDisplayMaterial'"));
	CameraDisplayMaterial = CameraDisplayMaterialFinder.Object;

	static ConstructorHelpers::FObjectFinder<UMaterial> WindowDisplayMaterialFinder(TEXT("Material'/VideoRecordingHelper/WindowDisplayMaterial.WindowDisplayMaterial'"));
	WindowDisplayMaterial = WindowDisplayMaterialFinder.Object;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> CameraModel(TEXT("StaticMesh'/VideoRecordingHelper/CameraModel.CameraModel'"));
	CameraMesh->SetStaticMesh(CameraModel.Object);
}

// Called when the game starts or when spawned
void ACamera::BeginPlay()
{
	Super::BeginPlay();

	RenderTarget = NewObject<UTextureRenderTarget2D>(this);
	RenderTarget->InitAutoFormat(RenderSize.X,RenderSize.Y);
	RenderTarget->ClearColor = FLinearColor::Red;
	RenderTarget->UpdateResource();

	SceneCapture->TextureTarget = RenderTarget;
	
	CameraDisplayMaterialDynamic = UMaterialInstanceDynamic::Create(CameraDisplayMaterial, this);
	WindowDisplayMaterialDynamic = UMaterialInstanceDynamic::Create(WindowDisplayMaterial, this);

	CameraDisplayMaterialDynamic->SetTextureParameterValue("Texture", RenderTarget);
	WindowDisplayMaterialDynamic->SetTextureParameterValue("Texture", RenderTarget);

	WindowBrush = new FSlateMaterialBrush(*WindowDisplayMaterialDynamic, RenderSize);

	CameraMesh->SetMaterial(1, CameraDisplayMaterialDynamic);

	CreateWindow();
}

void ACamera::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	RenderTarget->ReleaseResource();
	if(Window.IsValid()) Window->HideWindow();
}

void ACamera::CreateWindow()
{
	// Create Slate Window
	Window = SNew(SWindow)
	/* set all properties we got passed with the call */
		.Title(FText::FromString("Capture Window"))
		.ScreenPosition({0,0})
		.ClientSize(RenderSize)
		.UseOSWindowBorder(false)
		.bDragAnywhere(true)
		.CreateTitleBar(true)

	/* set a few more interesting ones, just to show they exist */
		.AutoCenter(EAutoCenter::None)
		.SaneWindowPlacement(true)
		.SizingRule(ESizingRule::UserSized)
		.Type(EWindowType::GameWindow)
		.InitialOpacity(1.0f)
		.HasCloseButton(true)
		.MaxHeight(9999)
		.MaxWidth(9999)
		.SupportsTransparency(EWindowTransparency::PerWindow)
		.LayoutBorder(FMargin {2, 2})
		.AdjustInitialSizeAndPositionForDPIScale(false)
	;

	// Add our new window to the Slate subsystem (which essentially opens it)
	FSlateApplication::Get().AddWindow(Window.ToSharedRef());

	// Make sure our new window doesn't hide behind some others
	Window.Get()->BringToFront(true);

	Image = SNew(SImage);
	Image->SetImage(WindowBrush);

	Window->SetContent(Image.ToSharedRef());
}

