# Description
This plugin is written to help with in-game video recordings. For this, it provides an actor that is shaped like a little camera and can be positioned in the world. On play, a second window is opened, which shows the view of the camera. This window can easily be recorded with something like [OBS](https://obsproject.com/de/download).

# Customization
You can change the rendering resolution of the camera in the actor itself. This also scales the output window accordingly. If you want to be able to grab the camera in-game, just derive a class from it and add your grabbing mechanism.
